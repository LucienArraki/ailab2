import AIEngine.DataFromFile;
import GameEngine.FinishGameListener;
import GameEngine.GameEngine;
import GameEngine.Skyscrapper.SkyscrapperEngine;
import GameEngine.TableListener;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.text.NumberFormat;
import java.util.List;

class AIGamePanel extends JFrame {

    private final int size;
    private GameEngine gameEngine;
    private JFormattedTextField[][] gameTable;

    AIGamePanel(int size) {
        super("Game");

        gameEngine = new SkyscrapperEngine();

        this.size = size;
        gameTable = new JFormattedTextField[size][size];

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(505, 500);

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Game");
        menuBar.add(menu);
        JMenuItem menuItem = new JMenuItem("END GAME");
        menuItem.addActionListener(new FinishGameListener(gameEngine));
        menu.add(menuItem);
        setJMenuBar(menuBar);


        Container topContainer = horizontalContainer(size, DataFromFile.topList);
        add(topContainer, BorderLayout.PAGE_START);

        Container leftContainer = verticalContainer(size, DataFromFile.leftList);
        add(leftContainer, BorderLayout.LINE_START);

        Container centerTable = centerContainer(size);
        gameEngine.setGameTextFieldTable(gameTable);
        add(centerTable, BorderLayout.CENTER);

        Container rightContainer = verticalContainer(size, DataFromFile.rightList);
        add(rightContainer, BorderLayout.LINE_END);

        Container downContainer = horizontalContainer(size, DataFromFile.downList);
        add(downContainer, BorderLayout.PAGE_END);

        setVisible(true);
    }

    private Container verticalContainer(int size, List<Integer> integerList) {
        Container verticalContainer = new Container();
        verticalContainer.setLayout(new GridLayout(size, 1));
        for (int i = 0; i < size; i++) {
            verticalContainer.add(getSkyscrapperParams(integerList.get(i)));
        }
        return verticalContainer;
    }

    private Container horizontalContainer(int size, List<Integer> integerList) {
        Container topTable = new Container();
        topTable.setLayout(new GridLayout(1, size));
        for (int i = 0; i < size; i++) {
            topTable.add(getSkyscrapperParams(integerList.get(i)));
        }
        return topTable;
    }

    private Container centerContainer(int size) {
        Container centerTable = new Container();
        centerTable.setLayout(new GridLayout(size, size, 5, 5));
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                JFormattedTextField textField = getTextField();
                centerTable.add(textField);
                gameTable[i][j] = textField;
            }
        }
        return centerTable;
    }

    private JLabel getSkyscrapperParams(int number) {
        JLabel jLabel = new JLabel(String.valueOf(number));
        jLabel.setFont(getGameFont());
        jLabel.setHorizontalAlignment(JTextField.CENTER);
        return jLabel;
    }

    private JFormattedTextField getTextField() {
        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter formatter = getNumberFormatter(format);
        JFormattedTextField jTextField = new JFormattedTextField(formatter);

        Font font = getGameFont();
        jTextField.setHorizontalAlignment(JTextField.CENTER);
        jTextField.setFont(font);
        jTextField.getDocument().addDocumentListener(new TableListener(gameEngine));
        jTextField.setBorder(BorderFactory.createEtchedBorder());
        return jTextField;
    }

    private NumberFormatter getNumberFormatter(NumberFormat format) {
        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(0);
        formatter.setMaximum(this.size);
        formatter.setAllowsInvalid(true);
        return formatter;
    }

    private Font getGameFont() {
        return new Font("SansSerif", Font.BOLD, 20);
    }
}
