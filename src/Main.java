import AIEngine.Engine;
import AIEngine.Futoshiki.Futoshiki;
import AIEngine.DataFromFile;

public class Main {
    public static void main(String[] args) {
        DataFromFile data = new DataFromFile();
        data.getDataFutoshiki();
//        AIGamePanel AIGamePanel = new AIGamePanel(DataFromFile.size);

//        SkyscrapperEngine skyscrapper = new SkyscrapperEngine(DataFromFile.size);
//        skyscrapper.checkRowAndColumn();
//        skyscrapper.backtracking(1);
//        System.out.println(skyscrapper.toString());

        Engine engine = new Futoshiki();
        engine.backtracking(0, 0, engine.getVariableMatrix());
    }
}
