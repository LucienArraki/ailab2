package GameEngine;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class GameEngine {

    protected JFormattedTextField[][] gameTextFieldTable;
    protected int[][] gameTable;
    protected boolean[][] booleanTable;

    public void setGameTextFieldTable(JFormattedTextField[][] gameTextFieldTable) {
        this.gameTextFieldTable = gameTextFieldTable;
        gameTable = new int[gameTextFieldTable.length][gameTextFieldTable.length];
        parseJTextFieldToIntTable();
        booleanTable = new boolean[gameTextFieldTable.length][gameTextFieldTable.length];
    }

    void parseJTextFieldToIntTable() {
        for (int i = 0; i < gameTextFieldTable.length; i++) {
            for (int j = 0; j < gameTextFieldTable.length; j++) {
                if (gameTextFieldTable[i][j].getValue() != null)
                    gameTable[i][j] = (int) gameTextFieldTable[i][j].getValue();
            }
        }
    }

    void parseIntTableToJTextField() {
        for (int i = 0; i < gameTextFieldTable.length; i++) {
            for (int j = 0; j < gameTextFieldTable.length; j++) {
                try {
                    gameTextFieldTable[i][j].setText("1");
                } catch (Exception e) {

                }
            }
        }
    }

    public boolean isFinished() {
        setBoolean();
        checkRowAndColumn();
        for (int i = 0; i < booleanTable.length; i++) {
            for (int j = 0; j < booleanTable.length; j++) {
                if (!booleanTable[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setBoolean() {
        for (int i = 0; i < booleanTable.length; i++) {
            for (int j = 0; j < booleanTable.length; j++) {
                booleanTable[i][j] = true;
            }
        }
    }

    public boolean backtracking(int counter) {
        return true;
    }

    public void checkRowAndColumn() {
        checkGame();
        for (int i = 0; i < gameTable.length; i++) {
            ArrayList<String> verticalNumbers = new ArrayList<>();
            ArrayList<String> horizontalNumbers = new ArrayList<>();
            for (int j = 0; j < gameTable.length; j++) {
                Object verticalObject = gameTable[i][j];
                checkElement(i, verticalNumbers, j, verticalObject);

                Object horizontalObject = gameTable[j][i];
                checkElement(j, horizontalNumbers, i, horizontalObject);
            }
        }
    }

    protected void checkGame() {
    }

    private void checkElement(int i, ArrayList<String> arrayList, int j, Object object) {
        if (object != null) {
            if (arrayList.indexOf(object.toString()) < 0) {
                arrayList.add(object.toString());
            } else {
                booleanTable[i][j] = false;
            }
        }
    }

    void setBlackColors() {
        for (int i = 0; i < gameTextFieldTable.length; i++) {
            for (int j = 0; j < gameTextFieldTable.length; j++) {
                gameTextFieldTable[i][j].setForeground(Color.BLACK);
                booleanTable[i][j] = true;
            }
        }
    }

    void setRedColors() {
        for (int i = 0; i < booleanTable.length; i++) {
            for (int j = 0; j < booleanTable.length; j++) {
                if (!booleanTable[i][j]) {
                    gameTextFieldTable[i][j].setForeground(Color.RED);
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int[] ints : gameTable) {
            s.append(Arrays.toString(ints)).append("\n");
        }
        return s.toString();
    }
}
