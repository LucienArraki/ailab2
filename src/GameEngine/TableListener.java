package GameEngine;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class TableListener implements DocumentListener {
    GameEngine gameEngine;

    public TableListener(GameEngine engine) {
        gameEngine = engine;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (gameEngine != null) {
            gameEngine.setBlackColors();
            gameEngine.parseJTextFieldToIntTable();
            gameEngine.checkRowAndColumn();
            gameEngine.setRedColors();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }
}