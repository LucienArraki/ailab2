package GameEngine.Skyscrapper;

import AIEngine.DataFromFile;
import GameEngine.GameEngine;

import java.util.ArrayList;
import java.util.List;

public class SkyscrapperEngine extends GameEngine {

    public SkyscrapperEngine() {
    }

    public SkyscrapperEngine(int size) {
        gameTable = new int[DataFromFile.size][DataFromFile.size];
        for (int i = 0; i < gameTable.length; i++) {
            for (int j = 0; j < gameTable.length; j++) {
                gameTable[i][j] = 1;
            }
        }
        booleanTable = new boolean[DataFromFile.size][DataFromFile.size];
    }

    public boolean backtracking(int counter) {
        if (isFinished()) {
            System.out.println(counter);
            return true;
        } else {
            for (int i = 0; i < booleanTable.length; i++) {
                for (int j = 0; j < booleanTable.length; j++) {
                    if (!booleanTable[i][j]) {
                        gameTable[i][j] = gameTable[i][j] == gameTable.length ? 1 : gameTable[i][j] + 1;
                    }
                    isFinished();
                }
            }
            backtracking(counter + 1);
        }
        return false;
    }

    @Override
    protected void checkGame() {
        for (int i = 0; i < gameTable.length; i++) {
            ArrayList<Integer> topArray = new ArrayList<>();
            int topNumber = 0;

            ArrayList<Integer> leftArray = new ArrayList<>();
            int leftNumber = 0;

            ArrayList<Integer> downArray = new ArrayList<>();
            int downNumber = 0;

            ArrayList<Integer> rightArray = new ArrayList<>();
            int rightNumber = 0;

            for (int j = 0; j < gameTable[i].length; j++) {
                leftNumber = getLeftNumber(i, leftArray, leftNumber, j);

                topNumber = getTopNumber(i, topArray, topNumber, j, DataFromFile.topList, booleanTable[j]);

                rightNumber = getRightNumber(i, rightArray, rightNumber, j);

                downNumber = getTopNumber(i, downArray, downNumber, gameTable.length - j - 1, DataFromFile.downList, booleanTable[j]);
            }
        }
    }

    private int getRightNumber(int i, ArrayList<Integer> rightArray, int rightNumber, int j) {
        rightNumber = getGreaterNumber(gameTable.length - j - 1, rightArray, rightNumber, i);
        if (DataFromFile.rightList.get(i) != 0 && rightArray.size() != DataFromFile.rightList.get(i)) {
            booleanTable[i][j] = false;
        }
        return rightNumber;
    }

    private int getTopNumber(int i, ArrayList<Integer> topArray, int topNumber, int j, List<Integer> topList, boolean[] booleans) {
        topNumber = getGreaterNumber(i, topArray, topNumber, j);
        if (DataFromFile.topList.get(i) != 0 && topArray.size() != topList.get(i)) {
            booleans[i] = false;
        }
        return topNumber;
    }

    private int getLeftNumber(int i, ArrayList<Integer> leftArray, int leftNumber, int j) {
        leftNumber = getGreaterNumber(j, leftArray, leftNumber, i);
        if (DataFromFile.leftList.get(i) != 0 && leftArray.size() != DataFromFile.leftList.get(i)) {
            booleanTable[i][j] = false;
        }
        return leftNumber;
    }

    private int getGreaterNumber(int i, ArrayList<Integer> gameArray, int selectedNumber, int j) {
        if (gameTable[j][i] != 0) {
            if (gameTable[j][i] > selectedNumber) {
                gameArray.add(gameTable[j][i]);
                selectedNumber = gameTable[j][i];
            }
            return selectedNumber;
        }
        return selectedNumber;
    }
}
