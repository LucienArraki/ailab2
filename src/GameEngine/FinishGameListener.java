package GameEngine;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FinishGameListener implements ActionListener {
    GameEngine gameEngine;

    public FinishGameListener(GameEngine engine) {
        gameEngine = engine;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("action");
        gameEngine.checkRowAndColumn();
        gameEngine.backtracking(1);
        System.out.println(gameEngine.toString());
    }
}
