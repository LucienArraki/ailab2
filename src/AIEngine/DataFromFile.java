package AIEngine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class DataFromFile {
    public static int size = 0;
    public static List<Integer> topList = new ArrayList<>();
    public static List<Integer> leftList = new ArrayList<>();
    public static List<Integer> rightList = new ArrayList<>();
    public static List<Integer> downList = new ArrayList<>();

    public static List<String[]> futoshiki = new ArrayList<>();
    public static int[][] futoshikiMatrix;

    public void getDataFutoshiki() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("./text/test_futo_4_0.txt"));
            String line;

            line = reader.readLine();
            size = Integer.parseInt(line);
            futoshikiMatrix = new int[size][size];
            while ((line = reader.readLine()) != null) {
                if (line.contains("START")) {
                    for (int i = 0; i < size; i++) {
                        line = reader.readLine();
                        String[] splited = line.split(";");
                        for (int j = 0; j < splited.length; j++) {
                            futoshikiMatrix[i][j] = Integer.parseInt(splited[j]);
                        }
                    }
                }
                if (line.contains("REL")) {
                    while ((line = reader.readLine()) != null) {
                        String[] splited = line.split(";");
                        futoshiki.add(splited);
                    }
                }
            }

            reader.close();
        } catch (Exception e) {
            System.err.format("futoshiki");
            e.printStackTrace();
        }
    }

    public void getDataSkyscrapper() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("./text/test_sky_4_0.txt"));
            String line;

            while ((line = reader.readLine()) != null) {
                if (line.length() > 0) {
                    if (line.substring(0, 1).equals("G")) {
                        String[] spilted = line.substring(2).split(";");
                        for (String s : spilted) {
                            topList.add(Integer.parseInt(s));
                        }
                    } else if (line.substring(0, 1).equals("D")) {
                        String[] spilted = line.substring(2).split(";");
                        for (String s : spilted) {
                            downList.add(Integer.parseInt(s));
                        }
                    } else if (line.substring(0, 1).equals("L")) {
                        String[] spilted = line.substring(2).split(";");
                        for (String s : spilted) {
                            leftList.add(Integer.parseInt(s));
                        }
                    } else if (line.substring(0, 1).equals("P")) {
                        String[] spilted = line.substring(2).split(";");
                        for (String s : spilted) {
                            rightList.add(Integer.parseInt(s));
                        }
                    } else {
                        size = Integer.parseInt(line);
                    }
                }
            }

            reader.close();
        } catch (Exception e) {
            System.err.format("sky");
            e.printStackTrace();
        }
    }
}
