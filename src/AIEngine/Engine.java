package AIEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public abstract class Engine {
    public int[][] gameMatrix;
    public Variable[][] variableMatrix;
    public Stack<Variable[][]> stack;

    public Engine() {
        stack = new Stack<>();
    }

    public Variable[][] getVariableMatrix() {
        return variableMatrix;
    }

    public abstract List<Field> createField();

    public void backtracking(int x, int y, Variable[][] variableMatrix) {
        if (checkMatrix(variableMatrix) || x == -1 || y == -1) {
            print(variableMatrix);
        } else {
            for (int i = 0; i < variableMatrix.length; i++) {
                for (int j = 0; j < variableMatrix.length; j++) {
                    if (variableMatrix[i][j].getFieldList().size() == 1) {
                        variableMatrix[i][j].setSelected(variableMatrix[i][j].getFieldList().get(0));
                    }
                }
            }

            int inc_x = x;
            int inc_y = y;

            if (y + 1 >= DataFromFile.size) {
                if (x + 1 >= DataFromFile.size) {
                    inc_x = -1;
                    inc_y = -1;
                } else {
                    inc_x += 1;
                    inc_y = 0;
                }
            } else {
                inc_y += 1;
            }

            for (int i = 0; i < variableMatrix[x][y].getFieldList().size(); i++) {
                variableMatrix[x][y].setSelected(variableMatrix[x][y].getFieldList().get(i));
                if (checkCell(x, y, variableMatrix)) {
                    backtracking(inc_x, inc_y, variableMatrix.clone());
                    print(variableMatrix);
                }
            }
            variableMatrix[x][y].setSelected(clear());

            if (isEmpty(variableMatrix)) {
                backtracking(inc_x, inc_y, variableMatrix);
            }
            if (isEmpty(variableMatrix)) {
                backtracking(0, 0, variableMatrix);
            }
        }
    }

    private boolean isEmpty(Variable[][] variableMatrix) {
        int[][] gameMatrix = setGameMatrixValue(variableMatrix);
        for (int i = 0; i < variableMatrix.length; i++) {
            for (int j = 0; j < variableMatrix.length; j++) {
                if (gameMatrix[i][j] != DataFromFile.futoshikiMatrix[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    protected abstract Field clear();

    private void print(Variable[][] variableMatrix) {
        for (int i = 0; i < variableMatrix.length; i++) {
            System.out.print("[ ");
            for (int j = 0; j < variableMatrix.length; j++) {
                System.out.print(variableMatrix[i][j].getSelected().getField() + " ");
            }
            System.out.println(']');
        }
        System.out.println();
    }


    public boolean checkCell(int x, int y, Variable[][] variableMatrix) {
        for (int i = 0; i < DataFromFile.size; i++) {
            if (i != x && variableMatrix[i][y].getSelected().getField() == variableMatrix[x][y].getSelected().getField()) {
                return false;
            }
            if (i != y && variableMatrix[x][i].getSelected().getField() == variableMatrix[x][y].getSelected().getField()) {
                return false;
            }
        }
        return chechGameCriteria(x, y, variableMatrix);
    }

    public abstract boolean chechGameCriteria(int x, int y, Variable[][] variableMatrix);


    public boolean checkMatrix(Variable[][] variableMatrix) {
        int[][] gameMatrix = setGameMatrixValue(variableMatrix);
        for (int i = 0; i < DataFromFile.size; i++) {
            List<Integer> vertical = new ArrayList<>();
            List<Integer> horizontal = new ArrayList<>();
            for (int j = 0; j < DataFromFile.size; j++) {
                if (vertical.indexOf(gameMatrix[i][j]) < 0) {
                    vertical.add(gameMatrix[i][j]);
                }
                if (vertical.indexOf(gameMatrix[i][j]) >= 0) {
                    return false;
                }
                if (horizontal.indexOf(gameMatrix[j][i]) < 0) {
                    horizontal.add(gameMatrix[j][i]);
                }
                if (horizontal.indexOf(gameMatrix[j][i]) >= 0) {
                    return false;
                }
                if (!chechGameCriteria(i, j, variableMatrix)) {
                    return false;
                }
            }
        }
        return true;
    }

    public int[][] setGameMatrixValue(Variable[][] variableMatrix) {
        int[][] gameMatrix = new int[DataFromFile.size][DataFromFile.size];
        for (int i = 0; i < DataFromFile.size; i++) {
            for (int j = 0; j < DataFromFile.size; j++) {
                gameMatrix[i][j] = variableMatrix[i][j].getSelected().getField();
            }
        }
        return gameMatrix;
    }
}
