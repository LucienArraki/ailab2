package AIEngine.Skyscrapper;

import AIEngine.DataFromFile;
import AIEngine.Engine;
import AIEngine.Field;
import AIEngine.Variable;

import java.util.ArrayList;
import java.util.List;

public class Skyscrapper extends Engine {
    public Skyscrapper() {
        this.gameMatrix = new int[DataFromFile.size][DataFromFile.size];
        this.variableMatrix = new Variable[DataFromFile.size][DataFromFile.size];
        List<Field> fieldList = createField();
        for (int i = 0; i < DataFromFile.size; i++) {
            for (int j = 0; j < DataFromFile.size; j++) {
                this.gameMatrix[i][j] = 0;
                this.variableMatrix[i][j] = new SkyscrapperVariable(i, j, new ArrayList<>(fieldList));
                this.variableMatrix[i][j].setSelected(new SkyscrapperField(0));

            }
        }
    }

    @Override
    public List<Field> createField() {
        List<Field> fieldList = new ArrayList<>();
        for (int i = 1; i <= DataFromFile.size; i++) {
            fieldList.add(new SkyscrapperField(i));
        }
        return fieldList;
    }

    @Override
    protected Field clear() {
        return new SkyscrapperField(0);
    }

    @Override
    public boolean chechGameCriteria(int x, int y, Variable[][] variableMatrix) {
        return true;
    }
}
