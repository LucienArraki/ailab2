package AIEngine.Skyscrapper;

import AIEngine.Field;
import AIEngine.Variable;

import java.util.List;

public class SkyscrapperVariable extends Variable {
    public SkyscrapperVariable(int x, int y, List<Field> fields) {
        super(x, y, fields);
    }
}
