package AIEngine;

import java.util.List;

public abstract class Variable {
    public List<Field> fieldList;
    int x;
    int y;
    Field selected;

    public Variable(int x, int y, List<Field> fields) {
        this.x = x;
        this.y = y;
        this.fieldList = fields;
        selected = fields.get(0);
    }

    public Field getSelected() {
        return selected;
    }

    public void setSelected(Field selected) {
        this.selected = selected;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public List<Field> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<Field> fieldList) {
        this.fieldList = fieldList;
    }
}
