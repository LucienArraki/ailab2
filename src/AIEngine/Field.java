package AIEngine;

public abstract class Field {
    int field;

    public Field(int field) {
        this.field = field;
    }

    public int getField() {
        return field;
    }
}
