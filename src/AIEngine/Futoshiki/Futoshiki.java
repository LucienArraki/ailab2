package AIEngine.Futoshiki;

import AIEngine.DataFromFile;
import AIEngine.Engine;
import AIEngine.Field;
import AIEngine.Skyscrapper.SkyscrapperField;
import AIEngine.Variable;

import java.util.ArrayList;
import java.util.List;

public class Futoshiki extends Engine {
    public Futoshiki() {
        this.gameMatrix = new int[DataFromFile.size][DataFromFile.size];
        this.variableMatrix = new Variable[DataFromFile.size][DataFromFile.size];
        List<Field> fieldList = createField();
        for (int i = 0; i < DataFromFile.size; i++) {
            for (int j = 0; j < DataFromFile.size; j++) {
                this.gameMatrix[i][j] = 0;
                int futoshikiMatrix = DataFromFile.futoshikiMatrix[i][j];
                if (futoshikiMatrix == 0) {
                    this.variableMatrix[i][j] = new FutoshikiVariable(i, j, new ArrayList<>(fieldList));
                } else {
                    List<Field> fields = new ArrayList<>();
                    fields.add(new SkyscrapperField(futoshikiMatrix));
                    this.variableMatrix[i][j] = new FutoshikiVariable(i, j, new ArrayList<>(fields));
                }
                this.variableMatrix[i][j].setSelected(new FutoshikiField(0));

            }
        }
    }

    @Override
    public List<Field> createField() {
        List<Field> fieldList = new ArrayList<>();
        for (int i = 1; i <= DataFromFile.size; i++) {
            fieldList.add(new FutoshikiField(i));
        }
        return fieldList;
    }

    @Override
    protected Field clear() {
        return new FutoshikiField(0);
    }

    @Override
    public boolean chechGameCriteria(int x, int y, Variable[][] variableMatrix) {
        for (String[] strings : DataFromFile.futoshiki) {
            int firstX = (int) strings[0].charAt(0) - 65;
            int firstY = Integer.parseInt(String.valueOf(strings[0].charAt(1))) - 1;
            int secondX = (int) strings[1].charAt(0) - 65;
            int secondY = Integer.parseInt(String.valueOf(strings[1].charAt(1))) - 1;
            if ((firstX == x && firstY == y) || (secondX == x && secondY == y))
                if (variableMatrix[firstX][firstY].getSelected().getField() > variableMatrix[secondX][secondY].getSelected().getField()) {
                    return false;
                }
        }
        return true;
    }
}
