package AIEngine.Futoshiki;

import AIEngine.Field;
import AIEngine.Variable;

import java.util.List;

public class FutoshikiVariable extends Variable {
    public FutoshikiVariable(int x, int y, List<Field> fields) {
        super(x, y, fields);
    }
}
